#!/bin/bash
if [[ "$(sudo systemctl status xl2tpd.service | grep '('running')')" ]]; then
  echo "l2tp status is OK"
  echo "----------------------------------------------"
  if [[ "$(sudo docker ps | grep configurator)" ]]; then
    sudo docker stop $(sudo docker ps -q)
    sudo systemctl stop docker
    systemctl stop mbpoller
    systemctl stop xl2tpd.service
    truncate -s 0 /var/lib/docker/containers/*/*-json.log
    STATUS_D="$(systemctl is-active docker)"
    STATUS_M="$(systemctl is-active mbpoller)"
    STATUS_X="$(systemctl is-active xl2tpd.service)"
    STATUS_W="$(systemctl is-active watchdog.service)"
    if [[ "${STATUS_D}" == "inactive" ]] && [[ "${STATUS_M}" == "inactive" ]] && [[ "${STATUS_X}" == "inactive" ]] && [[ "${STATUS_W}" == "inactive" ]]; then
      echo "----------------------------"
      echo "docker is inactive"
      echo "mbpoller is inactive"
      echo "xl2tpd.service is inactive"
      echo "watchdog.service is inactive"
      # sudo armbian-config
      sudo rm -rf create_armbian.sh
      sudo wget https://gitlab.com/ab3al/new_nano_config_script/-/raw/dev/create_armbian.sh
      sudo chmod +x create_armbian.sh
      sudo ./create_armbian.sh force
    fi
  else
          echo "wait a bit more"
  fi
else
        echo "-----------------------------------------------"
        echo "l2tp status is not working"
        echo "-----------------------------------------------"
fi
