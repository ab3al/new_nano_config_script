#!/bin/bash
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root or sudo" 
  exit 1
fi

user_key="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDNY28tooWQxmgibGDFYGE8wrMkxM8LyPSiULSTHW2reFtKfEMnBUkUwXGMNdaujZr0xniGGr6wE2QDdzh/ex89Tl/1ckzyfhmI9LF3Qy2j4DXUY6v/F3ct1BmiKC4ekRHDYG7YGBrkuQ7L3UMxt55/rSb8YRQj20c0MNfZtgXlK0ah4/cUSfFk+S6mVqInGVNlyuS0ZhJJIiV1q1Nb25HD4aZjNgzOKzS0wBiB0T9H5QS/zbEd1/TdFn5e3r9Podj197lRSmcyJD3itt1JxZOKvdU7EmJNxVe4WOHSjDYAGcsCUSeI9aiawoBx5Wtn/4nneMS5eUzxfMiq9EqKpgd+FmcZfeUKR2oKNV56YpG5WPmgBcb7uBUhkrxpcoyG7MCiqbEnKv5a45Nzzn8kI6OL7/Q/BG8Yo0yuvg3ap0ySsVBt+NJEvZnnZ4FX0TIhbDwQ7T8uHx0F6TVj7Q49TRvBJmWOkRFMtgU0yODafWrwWRXPeQrSjqEg9oFUBiyoeA9rBVkXSErZWXIzru7X4Ojtn3TCT5GQL67nTPkuhX/zATlymLO/3NF6YLc2mkBI5IBXGq6hl2ZzKbF4gGLllRgcratEizY5zrZc8bsGewqeKJ8Gjg/g/1wt4JqUWAwedM4HpejbAyH3VOZNXJECScbrFddJWvchOpRTE8dE43nOTw== info@home.bi.group"
username="ch"
hostname="nano"
root_password="h@rdware"

city="astana"
version="old"
slave="no"
old_nano="no"

git_user="ab3al"
git_pass="AAasgitlab513648"

GREEN="\e[32m"
ENDCOLOR="\e[0m"

echo "Starting set hostname to nano"
sudo hostnamectl set-hostname "$hostname"
echo "Starting set your city"
PS3='Please enter your city:'
options=("Astana" "Almaty" "Shymkent" "Tashkent" "Atyrau" "Oral" "Slave nano" "For old nano")
select opt in "${options[@]}"
do
  case $opt in
    "Astana")
      city="astana"
      slave="no"
      break;;
    "Almaty")
      city="almaty"
      slave="no"
      break;;
    "Shymkent")
      city="shymkent"
      slave="no"
      break;;
    "Tashkent")
      city="tashkent"
      slave="no"
      break;;
    "Atyrau")
      city="atyrau"
      slave="no"
      break;;
    "Oral")
      city="uralsk"
      slave="no"
      break;;
    "Slave nano")
      slave="yes"
      break;;
    "For old nano")
      slave="no"
      old_nano="yes"
      break;;
  esac
done

echo "Selected city is $city"
echo "Starting set your version"
PS3='Please enter desired HA version:'
ha_ver=("old" "new")
select ver in "${ha_ver[@]}"
do
  case $ver in
    "old")
      version="old"
      break;;
    "new")
      version="new"
      break;;
  esac
done

echo "Selected version is $version"

echo -e "$root_password\n$root_password" | passwd root
sudo chage --expiredate -1 root
sudo rm -f /root/.not_logged_in_yet || true
echo "-------------------------"
echo "Starting create .ssh file"
if ! id -u "$username" > /dev/null 2>&1; then
  sudo adduser --home "/home/$username" --disabled-password --gecos "" "$username"
  sudo echo -e "$root_password\n$root_password" | passwd "$username"
  sudo chage --expiredate -1 "$username"
  if [ -n "$user_key" ]; then
    sudo mkdir -p "/home/$username/.ssh"
    sudo chmod 700 "/home/$username/.ssh"
    sudo echo "$user_key" > "/home/$username/.ssh/authorized_keys"
    sudo chown -R "$username:$username" "/home/$username/.ssh"
    sudo chmod 600 "/home/$username/.ssh/authorized_keys"
  fi
  sudo adduser "$username" sudo

  echo "Add ch to sudoers"
  sudo echo "ch ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers 
  echo "Finishing create .ssh file"
  echo "-------------------------"
  echo "Copy key to authorized_keys"
  sudo sed  '/AuthorizedKeysFile/s/^#//' -i /etc/ssh/sshd_config
  sudo service ssh restart
  echo "Finishing Copy"
  echo "SSH Service is OK"
  echo "-------------------------"
  echo "Starting uncomment AuthorizedKeysFile"

  if [ "$(sudo systemctl status ssh.service | grep '('running')')" ]; then
    echo "Disable Root Login"
    sudo sed '/PermitRootLogin/s/#&/g/' -i /etc/ssh/sshd_config
    sudo echo "PermitRootLogin no" >> /etc/ssh/sshd_config
    # sudo sed  '/PermitRootLogin/s/^#//' -i /etc/ssh/sshd_config && sudo sed -i -e "s/\(PermitRootLogin \).*/\1no/" /etc/ssh/sshd_config
    echo "Disable Password auth"
    sudo echo "PasswordAuthentication no" >> /etc/ssh/sshd_config
    # sudo sed -i -e '/PasswordAuthentication/s/^#//' /etc/ssh/sshd_config 
    # sudo sed -i -e "s/\(PasswordAuthentication \).*/\1no/" /etc/ssh/sshd_config 
    sudo service ssh restart
    if [ "$(sudo systemctl status ssh.service | grep '('running')')" ]; then
      echo "SSH service configured successfully"
    else
      echo "-----------------------------------------------"
      echo "ssh status is not working. Try sudo apt-get remove --purge openssh-server && sudo apt-get install openssh-server"
      echo "-----------------------------------------------"
      exit 1
    fi
  else
    echo "-----------------------------------------------"
    echo "ssh status is not working. Try sudo apt-get remove --purge openssh-server && sudo apt-get install openssh-server"
    echo "-----------------------------------------------"
    exit 1
  fi
fi
echo "-----------------------------------------------"
echo "Start enable uart2 usbhost2 usbhost3"
# newoverlays="=usbhost0 usbhost1 usbhost2 uart1 uart2"
# sed -i "s/^overlays=.*/overlays=$newoverlays/" /boot/armbianEnv.txt
sudo sed -i -e "s/\(overlays=\).*/\1uart1 uart2 usbhost0 usbhost1 usbhost2 usbhost3/" /boot/armbianEnv.txt
echo -e "${GREEN}Finished enable uart2 usbhost2 usbhost3${ENDCOLOR}"
echo "-----------------------------------------------"

if [ -d /usr/share/hassio ]; then
  exit 0
fi

echo -e "${GREEN}--------------Start installing docker--------------${ENDCOLOR}"
if [ $old_nano = 'no' ]; then
  sudo apt-get update -y
  sudo apt-get install ca-certificates curl gnupg
  sudo install -m 0755 -d /etc/apt/keyrings
  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

  echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  apt-get update -y
  sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
elif [ $old_nano = 'yes' ]; then
  sudo apt-get update --allow-releaseinfo-change -y

  apt-get install -y \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg-agent

  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

  add-apt-repository \
    "deb [arch=armhf] https://download.docker.com/linux/debian $(lsb_release -cs) stable"

  apt-get update -y

  apt-get install -y --no-install-recommends \
      docker-ce=5:19.03.15~3-0~debian-buster \
      docker-ce-cli=5:19.03.15~3-0~debian-buster \
      containerd.io=1.3.9-1
fi
echo -e "${GREEN}--------------Docker installed--------------${ENDCOLOR}"

echo -e "${GREEN}--------------Start installing needed packages for HASS--------------${ENDCOLOR}"
wait

apt install \
avahi-daemon \
avahi-utils \
apparmor \
jq \
wget \
curl \
udisks2 \
libglib2.0-bin \
network-manager \
dbus \
lsb-release \
systemd-journal-remote \
systemd-resolved -y
echo -e "${GREEN}--------------Finished installing needed packages for HASS--------------${ENDCOLOR}"

echo -e "${GREEN}--------------Start downloading config files for HASS from git--------------${ENDCOLOR}"

# if [ $version = 'new' ]; then
#   echo "nameserver 8.8.8.8" >> /etc/resolv.conf
#   wait
# fi
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
wait
mkdir -p /usr/share/hassio
mkdir -p /usr/share/hassio/share
cd /usr/share/hassio
if [ $old_nano = 'no' ]; then
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/hassdefault"
  mv /usr/share/hassio/hassdefault /usr/share/hassio/homeassistant
  if [ $slave = 'no' ]; then
    rm /usr/share/hassio/homeassistant/setup/setup_slave.tpl
  else
    rm /usr/share/hassio/homeassistant/setup/setup.tpl
    mv /usr/share/hassio/homeassistant/setup/setup_slave.tpl /usr/share/hassio/homeassistant/setup/setup.tpl
  fi
  rm -r /usr/share/hassio/homeassistant/.git
  mkdir hass-configs
  cd hass-configs
  if [ $version = 'old' ]; then
    git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/standart_configs/custom_components"
  else
    git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/standart_configs/new_custom_components.git"
    mv /usr/share/hassio/hass-configs/new_custom_components /usr/share/hassio/hass-configs/custom_components
  fi
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/standart_configs/secrets"
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/standart_configs/configuration"
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/standart_configs/customize"
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/standart_configs/standart"
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/standart_configs/chsetup_options"
  cd /usr/share/hassio/hass-configs/custom_components
  chown -R $username:$username /usr/share/hassio/hass-configs/custom_components
  git config pull.rebase false
  cd /usr/share/hassio/hass-configs/secrets
  chown -R $username:$username /usr/share/hassio/hass-configs/secrets
  git config pull.rebase false
  cd /usr/share/hassio/hass-configs/configuration
  chown -R $username:$username /usr/share/hassio/hass-configs/configuration
  git config pull.rebase false
  cd /usr/share/hassio/hass-configs/customize
  chown -R $username:$username /usr/share/hassio/hass-configs/customize
  git config pull.rebase false
  cd /usr/share/hassio/hass-configs/standart
  chown -R $username:$username /usr/share/hassio/hass-configs/standart
  git config pull.rebase false
  cd /usr/share/hassio/hass-configs/chsetup_options
  chown -R $username:$username /usr/share/hassio/hass-configs/chsetup_options
  git config pull.rebase false
  cp -r /usr/share/hassio/hass-configs/custom_components /usr/share/hassio/homeassistant
  cp /usr/share/hassio/hass-configs/secrets/secrets.yaml /usr/share/hassio/homeassistant
  cp /usr/share/hassio/hass-configs/configuration/configuration.yaml /usr/share/hassio/homeassistant
  cp /usr/share/hassio/hass-configs/customize/customize.yaml /usr/share/hassio/homeassistant
  cp -r /usr/share/hassio/hass-configs/standart /usr/share/hassio/homeassistant/ch
  cp /usr/share/hassio/hass-configs/chsetup_options/options.tpl /usr/share/hassio/homeassistant/setup
  cd /home/$username
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/nano-configs/default_config.git"
  chown -R $username:$username /home/$username/default_config
  cd /home/$username/default_config
  git config pull.rebase false
  mkdir /usr/share/hassio/homeassistant/zigbee2mqtt
  cp -r /home/$username/default_config/devices/ /usr/share/hassio/homeassistant/zigbee2mqtt/
  touch /usr/share/hassio/homeassistant/zigbee2mqtt/configuration.yaml
  cat > /usr/share/hassio/homeassistant/zigbee2mqtt/configuration.yaml << EOF
external_converters:
  - devices/heiman_WaterSensor2-EF-3.0.js
  - devices/heiman_PIRILLSensor-EF-3.0.js
  - devices/heiman_HS3HT-EFA-3.0.js
  - devices/heiman_ColorDimmerSw-EF-3.0.js
  - devices/orvibo_accumulator_curtain.js
EOF
  if [ $city != 'astana' ]; then
    cp /home/$username/default_config/hass/$city/latitude.yaml /usr/share/hassio/homeassistant/setup
    cp /home/$username/default_config/hass/$city/longitude.yaml /usr/share/hassio/homeassistant/setup
    cp /home/$username/default_config/hass/$city/time_zone.yaml /usr/share/hassio/homeassistant/setup
    cp /home/$username/default_config/hass/$city/setup.tpl /usr/share/hassio/homeassistant/setup
    cp /home/$username/default_config/hass/$city/base_url.yaml /usr/share/hassio/homeassistant
    cp /home/$username/default_config/hass/$city/package.yaml /usr/share/hassio/homeassistant/setup
    cp /home/$username/default_config/hass/$city/name.yaml /usr/share/hassio/homeassistant/setup
  fi
  if [ $city = 'tashkent' ]; then
    cp /home/$username/default_config/hass/$city/z2m.txt /usr/share/hassio/homeassistant
  fi
  cp -r /home/$username/default_config/mosquitto /usr/share/hassio/share
  cp /home/$username/default_config/git-pull.sh /home/$username
  chmod +x /home/$username/git-pull.sh
  # cp /home/$username/default_config/98-ch.conf /etc/sysctl.d
  # cp /home/$username/default_config/force-reboot /etc/cron.d
  cp /home/$username/default_config/update.sh /home/$username
  chmod +x /home/$username/update.sh
  cp /home/$username/default_config/compare_hass_versions.sh /home/$username
  chmod +x /home/$username/compare_hass_versions.sh
  cp /home/$username/default_config/latest_version.txt /home/$username
  cp /home/$username/default_config/l2tp.sh /home/$username
  chmod +x /home/$username/l2tp.sh
  cp /home/$username/default_config/l2tp_uz.sh /home/$username
  chmod +x /home/$username/l2tp_uz.sh
  cp /home/$username/default_config/mdns.sh /home/$username
  chmod +x /home/$username/mdns.sh
  cp /home/$username/default_config/firmware.sh /home/$username
  chmod +x /home/$username/firmware.sh
  cp /home/$username/default_config/manual_update.sh /home/$username
  chmod +x /home/$username/manual_update.sh
  cp /home/$username/default_config/city_change.sh /home/$username
  chmod +x /home/$username/city_change.sh
  cp /home/$username/default_config/autorestart_xl2tpd.sh /home/$username
  chmod +x /home/$username/autorestart_xl2tpd.sh
  cp /home/$username/default_config/old_ha_config_update.py /usr/share/hassio/homeassistant
  cp /home/$username/default_config/service_account_credentials.js /usr/share/hassio/homeassistant
  cp /home/$username/default_config/dashboard_script.py /usr/share/hassio/homeassistant
  cp /home/$username/default_config/50-usb-realtek-net.rules /etc/udev/rules.d
  sudo udevadm trigger
elif [ $old_nano = 'yes' ]; then
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/old_hassdefault.git"
  mv /usr/share/hassio/old_hassdefault /usr/share/hassio/homeassistant
  rm -r /usr/share/hassio/homeassistant/.git
  mkdir hass-configs
  cd hass-configs
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/standart_configs/old_standart.git"
  mv /usr/share/hassio/hass-configs/old_standart /usr/share/hassio/hass-configs/standart
  cp -r /usr/share/hassio/hass-configs/standart /usr/share/hassio/homeassistant/ch/standart
  chown -R $username:$username /usr/share/hassio/hass-configs/standart
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/hass-configs/standart_configs/old_custom_components.git"
  mv /usr/share/hassio/hass-configs/old_custom_components /usr/share/hassio/hass-configs/custom_components
  cp -r /usr/share/hassio/hass-configs/custom_components /usr/share/hassio/homeassistant/custom_components
  chown -R $username:$username /usr/share/hassio/hass-configs/custom_components
  cd /home/$username
  git clone "https://$git_user:$git_pass@gitlab.com/connected-home/nano-configs/old_default_config.git"
  mv /home/$username/old_default_config /home/$username/default_config
  chown -R $username:$username /home/$username/default_config
  cp -r /home/$username/default_config/mosquitto /usr/share/hassio/share
  cp /home/$username/default_config/l2tp.sh /home/$username
  chmod +x /home/$username/l2tp.sh
  cp /home/$username/default_config/git-pull.sh /home/$username
  chmod +x /home/$username/git-pull.sh
  cp /home/$username/default_config/update.sh /home/$username
  chmod +x /home/$username/update.sh
  cp /home/$username/default_config/mdns.sh /home/$username
  chmod +x /home/$username/mdns.sh
  cp /home/$username/default_config/firmware.sh /home/$username
  chmod +x /home/$username/firmware.sh
  cp /home/$username/default_config/50-usb-realtek-net.rules /etc/udev/rules.d
  sudo udevadm trigger
  cp /home/$username/default_config/old_ha_config_update.py /usr/share/hassio/homeassistant
fi

mkdir /home/$username/cron_logs
if [ $slave = 'no' ]; then
  cat > /etc/cron.d/git-pull << EOF
30 3 * * * $username /home/$username/update.sh cron.d > /home/$username/cron_logs/gitpull_logs.log 2>&1
EOF
  cat > /etc/cron.d/autorestart_xl2tpd << EOF
1 */2 * * * root /home/$username/autorestart_xl2tpd.sh > /var/log/autorestart_xl2tpd.log
EOF
fi

# cd /etc
# rm rc.local
# cat > /etc/rc.local << EOF
# #!/bin/sh -e
# #
# # rc.local
# #
# # This script is executed at the end of each multiuser runlevel.
# # Make sure that the script will "exit 0" on success or any other
# # value on error.
# #
# # In order to enable or disable this script just change the execution
# # bits.
# #
# # By default this script does nothing.
# /home/$username/update.sh rc.local > /var/log/rc_local.log &
# exit 0
# EOF
# chmod +x rc.local
echo -e "${GREEN}--------------Finished downloading config files for HASS from git--------------${ENDCOLOR}"

echo -e "${GREEN}--------------Start installing SUPERVISOR--------------${ENDCOLOR}"
cd /home/$username

if [ $old_nano = 'no' ]; then
  if [ $version = 'old' ]; then
    sed -i '1s/.*/PRETTY_NAME="Debian GNU\/Linux 11 (bullseye)"/' /etc/os-release
  else
    sed -i '1s/.*/PRETTY_NAME="Debian GNU\/Linux 12 (bookworm)"/' /etc/os-release
  fi
  wget https://github.com/home-assistant/os-agent/releases/download/1.5.1/os-agent_1.5.1_linux_armv7.deb
  sudo dpkg -i os-agent_1.5.1_linux_armv7.deb
  wget https://github.com/home-assistant/supervised-installer/releases/latest/download/homeassistant-supervised.deb
  apt install ./homeassistant-supervised.deb
elif [ $old_nano = 'yes' ]; then
  curl -sL https://gitlab.com/connected-home/homeassistant/hassio-installer/-/raw/master/hassio_install.sh | bash -s -- -m raspberrypi2 -c stable
fi

if [ $slave = 'no' ]; then
  cp /home/$username/default_config/base-url.path /etc/systemd/system && sudo systemctl enable base-url.path
  cp /home/$username/default_config/base-url-changed.service /etc/systemd/system && sudo systemctl enable base-url-changed.service
  echo -e "${GREEN}Start installing mosquitto${ENDCOLOR}"
  docker run -d --name mosquitto --restart=always -v /usr/share/hassio/share/mosquitto/:/mosquitto/config/ -v /var/data/mosquitto/:/mosquitto/data -v /var/log/mosquitto/:/mosquitto/log --net=host eclipse-mosquitto
else    # if slave_nano
  sudo systemctl stop avahi-daemon.service
  sudo systemctl stop avahi-daemon.socket
  sudo systemctl disable avahi-daemon.service
  sudo systemctl disable avahi-daemon.socket
  sudo systemctl mask avahi-daemon
fi

echo -e "${GREEN}--------------Start installing python and configuring mbpoller--------------${ENDCOLOR}"
if [ $old_nano = 'no' ]; then
  sudo apt install python3-pip -y
  sudo rm /usr/lib/python3.11/EXTERNALLY-MANAGED
  sudo python3 -m pip install --upgrade pip setuptools wheel requests gspread oauth2client net-tools
  # sudo apt-get install python3-pandas -y
  sudo ln -s /usr/bin/python3 /usr/bin/python
  
elif [ $old_nano = 'yes' ]; then
  sudo apt-get install -y python
  sudo apt-get install -y python-pip
  python -m pip install --upgrade pip setuptools wheel requests
fi
mkdir -p /opt/modbuspollerenv
cp -r /home/$username/default_config/modbuspoller /opt/modbuspollerenv
cd /opt/modbuspollerenv/modbuspoller
cp /opt/modbuspollerenv/modbuspoller/mbpoller.service /lib/systemd/system && sudo systemctl enable mbpoller
cp /opt/modbuspollerenv/modbuspoller/mbpollerwatch.service /lib/systemd/system && sudo systemctl enable mbpollerwatch
cp /opt/modbuspollerenv/modbuspoller/mbpollerwatch.path /lib/systemd/system && sudo systemctl enable mbpollerwatch.path
systemctl start mbpoller
systemctl start mbpollerwatch
systemctl start mbpollerwatch.path
python -m pip install -r requirements.txt
echo -e "${GREEN}--------------End of installing python and configuring mbpoller--------------${ENDCOLOR}"

cd /home/$username
if [ $slave = 'no' ] && [ $city != 'tashkent' ]; then
  echo -e "${GREEN}Start installing l2tp${ENDCOLOR}"
  ./l2tp.sh
elif [ $slave = 'no' ] && [ $city = 'tashkent' ]; then
  echo -e "${GREEN}Start installing l2tp${ENDCOLOR}"
  ./l2tp_uz.sh
fi

if [ $city = 'almaty' ] || [ $city = 'astana' ] || [ $city = 'shymkent' ]; then
  echo "Set timezone to Asia/Almaty"
  sudo timedatectl set-timezone "Asia/Almaty"
elif [ $city = 'tashkent' ]; then
  echo "Set timezone to Asia/Tashkent"
  sudo timedatectl set-timezone "Asia/Tashkent"
elif [ $city = 'atyrau' ]; then
  echo "Set timezone to Asia/Atyrau"
  sudo timedatectl set-timezone "Asia/Atyrau"
elif [ $city = 'uralsk' ]; then
  echo "Set timezone to Asia/Oral"
  sudo timedatectl set-timezone "Asia/Oral"
fi

# cat > /etc/cron.d/time-check << EOF
# * * * * * root [[ $(date +%Y) -lt 2021 ]] && systemctl --force reboot
# EOF

mkdir /mnt/rootfs
sudo adduser $username docker

sudo chown -R $username:$username /home/$username
sudo chown -R $username:$username /usr/share/hassio
sudo chown -R $username:$username /opt/modbuspollerenv
cp /home/$username/default_config/daemon.json /etc/docker

if [ $old_nano = 'no' ]; then
  rm /home/$username/homeassistant-supervised.deb
  rm /home/$username/os-agent_1.5.1_linux_armv7.deb
  # rm -r /etc/apt/keyrings
fi
touch /home/$username/version1_0.txt
rm /root/install_new_firmware.sh
